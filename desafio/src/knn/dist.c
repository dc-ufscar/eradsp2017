#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
 *  * pRNG based on http://www.cs.wm.edu/~va/software/park/park.html
 *   */
#define MODULUS    2147483647
#define MULTIPLIER 48271
#define DEFAULT    123456789

static long seed = DEFAULT;

long int Random(void)
/* ----------------------------------------------------------------
 * Random returns a pseudo-random real number uniformly distributed 
 * between 0.0 and 1.0. 
 * ----------------------------------------------------------------
 */
{
  const long Q = MODULUS / MULTIPLIER;
  const long R = MODULUS % MULTIPLIER;
        long t;

  t = MULTIPLIER * (seed % Q) - R * (seed / Q);
  if (t > 0) 
    seed = t;
  else 
    seed = t + MODULUS;
  return ((double) seed / MODULUS)*1000000;
}

/*
 * End of the pRNG algorithm
 */

double dist(int *p, int *q, int n) {
	double d=0;
	double di;
	int i;
	for(i=0;i<n;i++) {
		di=p[i]-q[i];
		d+=(di*di);
	}
	return sqrt(d);
}

int main() {

	long int N, Q;
	long int i, n;
	int **Qi, *P;
	int Mi;
	double Md, parc;

#ifdef DATE
	system("date");
#endif
	fscanf(stdin, "%ld %ld", &N, &Q);
	Qi = (int**)malloc(Q*sizeof(int*));
	if(Qi == NULL)
		exit(1);
	for(i=0;i<Q;i++) {
		Qi[i] = (int*)malloc(N*sizeof(int));
		if(Qi[i] == NULL)
			exit(1);
	}
	for(i=0;i<Q;i++) {
		for(n=0;n<N;n++) {
			Qi[i][n]=Random();//fscanf(stdin, "%d", Qi+i*N+n);
		}
	}
	P = (int*)malloc(N*sizeof(int));
	for(n=0;n<N;n++) {
		*(P+n)=Random();//fscanf(stdin, "%d", P+n);
	}
#ifdef DATE
	system("date");
#endif
	Mi=0;
	Md=dist(P, Qi[0], N);
#ifdef DEBUG
	fprintf(stdout, "%d-%lf\n", Mi, Md);
#endif
	for(i=1;i<Q;i++) {
		parc=dist(P, Qi[i], N);
		if(parc < Md) {
			Mi = i;
			Md = parc;
#ifdef DEBUG
			fprintf(stdout, "%d-%lf\n", Mi, Md);
#endif
		}
	}

	fprintf(stdout, "%d\n", Mi);

	free(P);
	for(i=0;i<Q;i++) {
		free(Qi[i]);
	}
	free(Qi);
#ifdef DATE
	system("date");
#endif

	return 0;
}
